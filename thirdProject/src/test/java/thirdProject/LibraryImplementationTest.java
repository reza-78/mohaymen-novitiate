package thirdProject;

import jdk.nashorn.internal.ir.annotations.Ignore;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import thirdProject.booksRelated.Book;
import thirdProject.membersRelated.Member;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class LibraryImplementationTest {
    public LibraryImplementationTest(){}
    static LibraryImplementation library =new LibraryImplementation();
    @BeforeAll
     static void addSomeMembers(){
        library.members.put(0L,new Member("ali goli", (short) 20,Gender.FEMALE,0));
        library.members.put(1L,new Member("reza mehtari", (short) 21,Gender.MALE,1));
        library.members.put(2L,new Member("mehdi setayande", (short) 21,Gender.MALE,2));
        library.members.put(3L,new Member("erfan nuriyan", (short) 21,Gender.MALE,3));
        library.members.put(4L,new Member("sadra hakim", (short) 20,Gender.MALE,4));
        library.members.put(5L,new Member("alireza mehrnazar", (short) 20,Gender.FEMALE,5));

        library.books.put(0L,new Book("math",0L)  );
        library.books.put(1L,new Book("math2",1L)  );
        library.books.put(2L,new Book("math3",2L)  );
        library.books.put(3L,new Book("math4",3L)  );
    }

    @Test
    void findContainsName() {
        List<Entity> entityArray =new ArrayList<>();
        entityArray.add(library.members.get(0L));
        entityArray.add(library.members.get(5L));
        assertArrayEquals(entityArray.toArray(),library.searchByNameContains("ali").toArray());

    }

    @Test
    void removeBookTest() {
        try {
            library.removeBook(3);
        } catch (EntityNotFoundException e) {
            e.printStackTrace();
        }
        assertFalse(library.books.containsKey(3L));
    }
    @Test
    void showMember(){
        try {
            library.show(8);
            fail();
        } catch (EntityNotFoundException e) {
            System.out.println(e.getMessage());
        }
    }

    @Ignore
    @Test
    void extendTest(){

    }

    @AfterAll
    static void removeLibraryObject(){
        library=null;
        System.gc();
    }
}
@RunWith(Suite.class)
@Suite.SuiteClasses({Book.class,Member.class})
class BookAndMemberTest{

}
/*
* builder
* data
* value
* */
