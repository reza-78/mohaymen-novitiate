package thirdProject;

public class BadEntityException extends Exception{
    BadEntityException(){
        super();
    }
    BadEntityException(String message){
        super(message);
    }
}
