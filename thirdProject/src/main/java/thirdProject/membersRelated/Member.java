package thirdProject.membersRelated;


import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import thirdProject.Gender;
import thirdProject.Person;
import thirdProject.booksRelated.Book;

import java.util.ArrayList;
import java.util.List;
@Setter(AccessLevel.PUBLIC)
@Getter(lazy = true)

@ToString

public class Member extends Person {
    long id;
    private List<Book> borrowedBooksList = new ArrayList<>();

    public Member(String name, short age, Gender gender, long id) {
        super(name,age,gender);
        this.id = id;

    }
    public Member(){}

    public void addBook(Book book) {
        borrowedBooksList.add(book);
    }

    public void removeBook(Book remove) {
        if (borrowedBooksList.remove(remove))
            System.out.println("removed");
        else
            System.out.println("not exist");
        borrowedBooksList.forEach(v -> System.out.println(v));
    }

    public void setGender(String gender){
        this.gender=gender.equalsIgnoreCase("male")? Gender.MALE: Gender.FEMALE;;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void showOnConsole() {
        System.out.println(toString());
    }
}
