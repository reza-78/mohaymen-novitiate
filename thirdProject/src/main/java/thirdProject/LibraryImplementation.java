package thirdProject;

import thirdProject.booksRelated.Book;
import thirdProject.membersRelated.Member;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * <h1>Library Project</h1>
 * <p>in this project you can manage a library</p>
 *
 * @author Reza Mehtari
 * @version 1.0
 * @see Book
 * @see Member
 * @since 2020-7-26
 */


interface Library {
    void save(Entity entity);

    void update(Entity e) throws EntityNotFoundException;

    void delete(Entity e) throws EntityNotFoundException;

    void borrow(Member member, Book book);

    void giveBack(Member member, Book book);
}

public class LibraryImplementation implements Library {
    static long id = 0;
    Map<Long, Member> members = new HashMap<>();
    Map<Long, Book> books = new HashMap<>();
    Scanner s = new Scanner(System.in);


    //----------------------------- about members ------------------------------

    void memberOperation() throws BadEntityException, EntityNotFoundException {
        System.out.println("1.add member");
        System.out.println("2.remove member");
        System.out.println("3.show information");
        System.out.println("4.edit information");
        int order = s.nextInt();
        switch (order) {

            case 1:
                s.nextLine();
                Entity newMember = new Member();
                newMember.readFromConsole();
                save(newMember);
                break;
            case 2:
                System.out.println("enter id");
                delete(members.get(s.nextLong()));
                break;
            case 3:
                System.out.println("enter id");
                show(s.nextLong());
                break;
            case 4:
                System.out.println("enter id");
                update(members.get(s.nextLong()));
                break;
            default:
                System.out.println("wrong input");
        }
    }

    //add new member
    @Override
    public void save(Entity newMember) {
        ((Member)newMember).setId(id);
        members.put(id, (Member) newMember);
        System.out.println("your id : " + (id));
        id++;
    }

    //remove a member
    @Override
    public void delete(Entity member) throws EntityNotFoundException {
        if (member != null) {
            members.remove(member);
            System.out.println("removed correctly");
        } else
            throw new EntityNotFoundException("member not found");
    }

    //show members information
    Member show(long id) throws EntityNotFoundException {
        if (members.containsKey(id)) {
            return members.get(id);
        } else
            throw new EntityNotFoundException("member not found");
    }

    //edit information
    @Override
    public void update(Entity memberUpdate) throws EntityNotFoundException {
        if (memberUpdate == null)
            throw new EntityNotFoundException("member not found ");
        else {
            Member member = members.get(memberUpdate.getId());
            System.out.println("enter name: ");
            s.nextLine();
            member.setName(s.nextLine());
            System.out.println("enter age: ");
            member.setAge(s.nextShort());
            System.out.println("enter gender");
            member.setGender(s.next());
            System.out.println(members.get(member.getId()));
        }
    }

    //--------------------------- about books ---------------------------------

    void bookOperation() throws EntityNotFoundException {
        System.out.println("1.add book");
        System.out.println("2.remove book");
        System.out.println("3.edit book");
        System.out.println("4.show book");
        int order = s.nextInt();
        switch (order) {
            case 1:
                System.out.println("enter name , id");
                s.nextLine();
                addBook(s.nextLine(), s.nextLong());
                break;
            case 2:
                System.out.println("enter bookId");
                s.nextLine();
                removeBook(s.nextLong());
                break;
            case 3:
                System.out.println("enter book's id");
                s.nextLine();
                editBook(s.nextLong());
                break;
            case 4:
                System.out.println("enter book's id");
                s.nextLine();
                showBook(s.nextLong());
            default:
                System.out.println("wrong input");
        }
    }

    //add new book
    private void addBook(String name, long id) {
        books.put(id, new Book(name, id));
        System.out.println("book added");
    }
    /**
     * @throws EntityNotFoundException if book not exist
     */

    //remove book
    void removeBook(long id) throws EntityNotFoundException {
        if (books.remove(id, books.get(id)))
            System.out.println("removed correctly");
        else
            throw new EntityNotFoundException("book not exist");
    }

    //show books if exist
    private Book showBook(long id) throws EntityNotFoundException {
        if (books.containsKey(id))
            return books.get(id);
        else
            throw new EntityNotFoundException("book not exist");
    }

    private void editBook(long id) throws EntityNotFoundException {
        if (!books.containsKey(id))
            throw new EntityNotFoundException("book not exist");
        else {
            s.nextLine();
            System.out.println("enter book's name");
            Book bookForEdit = books.get(id);
            bookForEdit.setName(s.nextLine());
            System.out.println("enter book's id");
            bookForEdit.setId(s.nextLong());
            System.out.println(bookForEdit + " edited");
        }
    }

    //--------------------------------search-------------------------------------

    void searchOptions() {
        System.out.println("1.search members exactly");
        System.out.println("2.search members that contain chosen name");
        System.out.println("3.search who borrowed the ");
        System.out.println("4.search in books exactly");
        System.out.println("5.search in books that contain chosen name");
        int order = s.nextInt();
        switch (order) {
            case 1:
                System.out.println("enter name");
                s.nextLine();
                searchByNameExactly(s.nextLine());
                break;
            case 2:
                System.out.println("enter name");
                s.nextLine();
                searchByNameContains(s.nextLine());
                break;
            case 3:
                System.out.println("enter bookId");
                searchWhoBorrowed(s.next());
                break;
            case 4:
                System.out.println("enter the book name");
                s.nextLine();
                searchInBooksExactly(s.nextLine());
                break;
            case 5:
                System.out.println("enter the name");
                s.nextLine();
                searchBooksByNameContains(s.nextLine());
                break;
            default:
                System.out.println("wrong input");
        }
    }

    //return the member that is borrowed the book
    private void searchWhoBorrowed(String bookId) {
        System.out.println(members.get(books.get(bookId).getPersonThatBorrowedId()));
    }

    //search exactly members by name
    private void searchByNameExactly(String name) {
        ArrayList<Entity> found = new ArrayList<>();
        members.forEach((k, v) -> {
            if (v.getName().equals(name)) {
                found.add(v);
                System.out.println(v);
            }
        });
    }

    //search members by name that contains the name
     ArrayList<Entity> searchByNameContains(String name) {
        ArrayList<Entity> found = new ArrayList<>();
        members.forEach((k, v) -> {
            if (v.getName().contains(name)) {
                found.add(v);
                System.out.println(v);
            }
        });
        return found;
    }

    //search books by name exactly
    private void searchInBooksExactly(String name) {
        books.forEach((k, v) -> {
            if (v.getName().equals(name))
                System.out.println(v);
        });
    }

    //search books by name and print all books that their name contain the input name
    private void searchBooksByNameContains(String name) {
        books.forEach((k, v) -> {
            if (v.getName().contains(name))
                System.out.println(v);
        });
    }
    //-------------------------other operations ----------------------------

    //borrow books
    @Override
    public void borrow(Member member, Book book) {

        if (books.containsKey(book.getId())) {
            if (books.get(book.getId()).getPersonThatBorrowedId() == -1) {
                book.setPersonThatBorrowedId(member.getId());
                book.setBorrowDate(true);
                members.get(member.getId()).addBook(book);

            } else
                System.out.println("lent");
        } else
            System.out.println("not exist");
    }

    //deliver a book
    @Override
    public void giveBack(Member member, Book book) {
        book.setPersonThatBorrowedId(-1);
        book.setBorrowDate(false);
        members.get(member.getId()).removeBook(book);
    }

    /**
     * @return list of members that should pay fine
     */
    //find members that should pay fine
    ArrayList<Person> payFine() {
        ArrayList<Person> shouldPayFine = new ArrayList<>();
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
        final String now = dateTimeFormatter.format(LocalDateTime.now());
        books.forEach((k, v) -> {
            if (compareDate(now, v.getBorrowDate()) && v.getBorrowDate() != null)
                shouldPayFine.add(members.get(v.getPersonThatBorrowedId()));
        });
        return shouldPayFine;
    }

    //extend (tamdid)
    void extend(String bookId) {
        books.get(bookId).setBorrowDate(true);
    }

    /**
     * @param now        is today's date
     * @param borrowDate is date that member is borrowed the book
     */

    //compare borrow date and now for finding members should pay fine
    private boolean compareDate(String now, String borrowDate) {
        int yearNow = Integer.parseInt(now.substring(0, 4));
        int yearBorrowDate = Integer.parseInt(borrowDate.substring(0, 4));

        int monthNow = now.charAt(5) == '0' ? now.charAt(6) - 48 : Integer.parseInt(now.substring(5, 7));
        int monthBorrow = borrowDate.charAt(5) == '0' ? borrowDate.charAt(6) - 48 : Integer.parseInt(borrowDate.substring(5, 7));

        int dayNow = now.charAt(8) == '0' ? now.charAt(9) - 48 : Integer.parseInt(now.substring(8, 10));
        int dayBorrow = borrowDate.charAt(8) == '0' ? borrowDate.charAt(9) - 48 : Integer.parseInt(borrowDate.substring(8, 10));

        //20 days allowed
        if ((yearNow * 356 + monthNow * 30 + dayNow) - (yearBorrowDate * 365 + monthBorrow * 30 + dayBorrow) > 20)
            return true;
        else
            return false;
    }
}


class EntityNotFoundException extends Exception {
    EntityNotFoundException() {
        super();
    }

    EntityNotFoundException(String message) {
        super(message);
    }
}

