package thirdProject;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import java.util.Scanner;
import java.util.regex.Pattern;
@Setter(AccessLevel.PUBLIC)
@Getter(AccessLevel.NONE)
abstract public class Person implements Entity{
    protected String name;
    protected short age;
    protected Gender gender;
    private Scanner scanner = new Scanner(System.in);

    public Person(String name, short age, Gender gender) {
        this.name = name;
        this.age = age;
        this.gender=gender;
    }
    public Person(){}

    @Override
    public void readFromConsole() throws BadEntityException {

        System.out.println("enter your name ");
        name=scanner.nextLine();
        if (!Pattern.matches("[a-zA-Z\\s]{6,15}",name))
            throw new BadEntityException("name is invalid");
        System.out.println("enter your age");
        age=scanner.nextShort();
        if (!Pattern.matches("[0-9]{1,3}",String.valueOf(age)))
            throw new BadEntityException("invalid age");
        System.out.println("enter gender");
        String inputGender =scanner.next();
        gender=inputGender.equalsIgnoreCase("male")? Gender.MALE: Gender.FEMALE;
    }


    public String getName() {
        return name;
    }
}
