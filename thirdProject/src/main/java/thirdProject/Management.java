package thirdProject;

import java.util.Scanner;

public class Management {
    public static void main(String[] args) {
        LibraryImplementation library = new LibraryImplementation();
        Scanner s =new Scanner(System.in);
        while (true) {
            System.out.println("1.members");
            System.out.println("2.books");
            System.out.println("3.search");
            System.out.println("4.borrow");
            System.out.println("5.deliver");
            System.out.println("6.extend");
            System.out.println("7.find members should pay fine");
            System.out.println("8.exit");
            int chosen = s.nextInt();
            switch (chosen) {

                //members
                case 1:
                    try {
                        library.memberOperation();
                    } catch (BadEntityException | EntityNotFoundException e) {
                        System.out.println(e.getMessage());
                    }
                    break;

                //book
                case 2:
                    try {
                        library.bookOperation();
                    } catch (EntityNotFoundException e) {
                        System.out.println(e.getMessage());
                    }
                    break;

                //search
                case 3:
                    library.searchOptions();
                    break;

                //borrow books
                case 4:
                    System.out.println("enter member id , enter book id");
                    library.borrow(library.members.get(s.nextLong()),library.books.get(s.nextLong()) );
                    break;

                //deliver books
                case 5:
                    System.out.println("enter member id , enter book id");

                    library.giveBack(library.members.get(s.nextLong()) ,library.books.get(s.nextLong()) );
                    break;

                //extend (tamdid)
                case 6:
                    System.out.println("enter book id");
                    library.extend(s.next());
                    break ;

                //pay fine
                case 7:
                    System.out.println("members should pay fine");
                    library.payFine().forEach(System.out::println);
                    break ;

                //exit
                case 8:
                    return;
                default:
                    System.out.println("wrong input");
            }
        }
    }
}
