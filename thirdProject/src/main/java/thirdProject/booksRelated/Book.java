package thirdProject.booksRelated;
import thirdProject.BadEntityException;
import thirdProject.Entity;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class Book implements Entity{
    private Scanner scanner = new Scanner(System.in);
    private String name;
    private long id;
    private long personThatBorrowedId = -1;
    private String borrowDate;
    final private DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");

    public Book(String name, long id) {
        this.name = name;
        this.id = id;
    }

    public void setBorrowDate(boolean borrow) {
        if (!borrow)
            borrowDate=null;
        else
            this.borrowDate = dateTimeFormatter.format(LocalDateTime.now());
    }

    public String getBorrowDate() {
        return borrowDate;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public long getId() {
        return id;
    }

    public void setPersonThatBorrowedId(long personThatBorrowedId) {
        this.personThatBorrowedId = personThatBorrowedId;
    }

    public long getPersonThatBorrowedId() {
        return personThatBorrowedId;
    }

    @Override
    public String toString() {
        return "Book{" +
                "name='" + name + '\'' +
                ", id='" + id + '\'' +
                ", personThatBorrowedId=" + personThatBorrowedId +
                '}';
    }

    @Override
    public void showOnConsole() {
        System.out.println(toString());
    }

    @Override
    public void readFromConsole() throws BadEntityException {
        System.out.println("enter book name");
        name=scanner.nextLine();
        scanner.nextLine();
        System.out.println("enter id");
        id= scanner.nextLong();
    }
}

