package thirdProject;

public interface Entity {
    long getId();
    void readFromConsole() throws BadEntityException;
    void showOnConsole();
}
