package membersRelated;
import booksRelated.Book;

import java.util.ArrayList;
import java.util.List;

public class Person {
    private String name;
    private short age;
    private char gender;
    private long id;
    private List<Book> borrowedBooksList = new ArrayList<>();

    public Person(String name, short age, char gender, long id) {
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.id = id;
    }

    public void addBook(Book book) {
        borrowedBooksList.add(book);
    }

    public void removeBook(Book remove) {
        if (borrowedBooksList.remove(remove))
            System.out.println("removed");
        else
            System.out.println("not exist");
        borrowedBooksList.forEach(v -> System.out.println(v));
    }

    public void setAge(short age) {
        this.age = age;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", gender=" + gender +
                '}';
    }
}
