package booksRelated;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Book {
    private String name;
    private String id;
    private long personThatBorrowedId = -1;
    private String borrowDate;
    final private DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");

    public Book(String name, String id) {
        this.name = name;
        this.id = id;
    }

    public void setBorrowDate(boolean borrow) {
        if (!borrow)
            borrowDate=null;
        else
            this.borrowDate = dateTimeFormatter.format(LocalDateTime.now());
    }

    public String getBorrowDate() {
        return borrowDate;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public void setPersonThatBorrowedId(long personThatBorrowedId) {
        this.personThatBorrowedId = personThatBorrowedId;
    }

    public long getPersonThatBorrowedId() {
        return personThatBorrowedId;
    }

    @Override
    public String toString() {
        return "Book{" +
                "name='" + name + '\'' +
                ", id='" + id + '\'' +
                ", personThatBorrowedId=" + personThatBorrowedId +
                '}';
    }
}
