import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import booksRelated.Book;
import membersRelated.Person;

/**
 * <h1>Library Project</h1>
 * <p>in this project you can manage a library</p>
 *
 * @author Reza Mehtari
 * @version 1.0
 * @since 2020-7-26
 * @see booksRelated.Book
 * @see membersRelated.Person
*/

public class Library {
    static long id = 0;
    static Map<Long, Person> members;
    static Map<String, Book> books;
    static Scanner s = new Scanner(System.in);

    public static void main(String[] args) {
        members = new HashMap<>();
        books = new HashMap<>();
        while (true) {
            System.out.println("1.members");
            System.out.println("2.books");
            System.out.println("3.search");
            System.out.println("4.borrow");
            System.out.println("5.deliver");
            System.out.println("6.extend");
            System.out.println("7.find members should pay fine");
            System.out.println("8.exit");
            int chosen = s.nextInt();
            switch (chosen) {

                //members
                case 1:
                    memberOperation();
                    break;

                    //book
                case 2:
                    bookOperation();
                    break;

                    //search
                case 3:
                    searchOptions();
                    break;

                    //borrow books
                case 4:
                    System.out.println("enter member id , enter book id");
                    borrow(s.nextLong(), s.next());
                    break;

                    //deliver books
                case 5:
                    System.out.println("enter member id , enter book id");
                    deliver(s.nextLong(), s.next());
                    break;

                    //extend (tamdid)
                case 6:
                    System.out.println("enter book id");
                    extend(s.next());
                    break ;

                    //pay fine
                case 7:
                    System.out.println("members should pay fine");
                    payFine().forEach(System.out::println);
                    break ;

                    //exit
                case 8:
                    return;
                default:
                    System.out.println("wrong input");
            }
        }
    }
    //----------------------------- about members ------------------------------

    private static void memberOperation(){
        System.out.println("1.add member");
        System.out.println("2.remove member");
        System.out.println("3.show information");
        System.out.println("4.edit information");
        int order = s.nextInt();
        switch (order) {

            case 1:
                s.nextLine();
                System.out.println("enter name , age , gender");
                addMember(s.nextLine(), s.nextShort(), s.next().charAt(0));
                break;
            case 2:
                System.out.println("enter id");
                removeMember(s.nextLong());
                break;
            case 3:
                System.out.println("enter id");
                show(s.nextLong());
                break;
            case 4:
                System.out.println("enter id");
                edit(s.nextLong());
                break;
            default:
                System.out.println("wrong input");
        }
    }

    //add new member
    private static void addMember(String name, short age, char gender) {
        members.put(id, new Person(name, age, gender, id++));
        System.out.println("your id : " + (id - 1));
    }

    //remove a member
    private static void removeMember(long id) {
        if (members.remove(id, members.get(id))) {
            System.out.println("removed correctly");
        } else
            System.out.println("not exist");
    }

    //show members information
    private static void show(long id) {
        if (members.containsKey(id)) {
            System.out.println(members.get(id));
        } else
            System.out.println("not exist");
    }

    //edit information
    private static void edit(long id) {
        if (!members.containsKey(id))
            System.out.println("not exist");
        else {
            Person person = members.get(id);
            System.out.println("enter name: ");
            s.nextLine();
            person.setName(s.nextLine());
            System.out.println("enter age: ");
            person.setAge(s.nextShort());
            System.out.println("enter gender");
            person.setGender(s.next().charAt(0));
            System.out.println(members.get(id));
        }
    }

    //--------------------------- about books ---------------------------------

    private static void bookOperation(){
        System.out.println("1.add book");
        System.out.println("2.remove book");
        System.out.println("3.edit book");
        System.out.println("4.show book");
        int order = s.nextInt();
        switch (order) {
            case 1:
                System.out.println("enter name , id");
                s.nextLine();
                addBook(s.nextLine(), s.next());
                break;
            case 2:
                System.out.println("enter bookId");
                s.nextLine();
                removeBook(s.nextLine());
                break ;
            case 3:
                System.out.println("enter book's id");
                s.nextLine();
                editBook(s.nextLine());
                break;
            case 4:
                System.out.println("enter book's id");
                s.nextLine();
                showBook(s.nextLine());
            default:
                System.out.println("wrong input");
        }
    }

    //add new book
    private static void addBook(String name, String id) {
        books.put(id, new Book(name, id));
        System.out.println("book added");
    }

    //remove book
    private static void removeBook(String id){
        if (books.remove(id,books.get(id)))
            System.out.println("removed correctly");
        else
            System.out.println("not exist");
    }

    //show books if exist
    private static void showBook(String id){
        if (books.containsKey(id))
            System.out.println(books.get(id));
        else
            System.out.println("not exist");
    }

    private static void editBook(String id){
        if (!books.containsKey(id))
            System.out.println("not exist");
        else {
            System.out.println("enter book's name");
            Book bookForEdit =books.get(id);
            bookForEdit.setName(s.nextLine());
            System.out.println("enter book's id");
            bookForEdit.setId(s.nextLine());
            System.out.println(bookForEdit + " edited");
        }
    }

    //--------------------------------search-------------------------------------

    private static void searchOptions(){
        System.out.println("1.search members exactly");
        System.out.println("2.search members that contain chosen name");
        System.out.println("3.search who borrowed the ");
        System.out.println("4.search in books exactly");
        System.out.println("5.search in books that contain chosen name");
        int order = s.nextInt();
        switch (order) {
            case 1:
                System.out.println("enter name");
                s.nextLine();
                searchByNameExactly(s.nextLine());
                break;
            case 2:
                System.out.println("enter name");
                s.nextLine();
                searchByNameContains(s.nextLine());
                break;
            case 3:
                System.out.println("enter bookId");
                searchWhoBorrowed(s.next());
                break;
            case 4:
                System.out.println("enter the book name");
                s.nextLine();
                searchInBooksExactly(s.nextLine());
                break ;
            case 5:
                System.out.println("enter the name");
                s.nextLine();
                searchBooksByNameContains(s.nextLine());
                break ;
            default:
                System.out.println("wrong input");
        }
    }

    //return the member that is borrowed the book
    private static void searchWhoBorrowed(String bookId) {
        System.out.println(members.get(books.get(bookId).getPersonThatBorrowedId()));
    }

    //search exactly members by name
    private static void searchByNameExactly(String name) {
        members.forEach((k, v) -> {
            if (v.getName().equals(name)) {
                System.out.println(v);
            }
        });
    }

    //search members by name that contains the name
    private static void searchByNameContains(String name) {
        members.forEach((k, v) -> {
            if (v.getName().contains(name)) {
                System.out.println(v);
            }
        });
    }
    //search books by name exactly
    private static void searchInBooksExactly(String name){
        books.forEach((k,v) -> {
            if (v.getName().equals(name))
                System.out.println(v);
        });
    }

    //search books by name and print all books that their name contain the input name
    private static void searchBooksByNameContains(String name){
        books.forEach((k,v)->{
            if (v.getName().contains(name))
                System.out.println(v);
        });
    }
    //-------------------------other operations ----------------------------

    //borrow books
    private static void borrow(long id, String bookId) {
        if (books.containsKey(bookId)) {
            if (books.get(bookId).getPersonThatBorrowedId() == -1) {
                Book bookForBorrow =books.get(bookId);
                bookForBorrow.setPersonThatBorrowedId(id);
                bookForBorrow.setBorrowDate(true);
                members.get(id).addBook(bookForBorrow);

            } else
                System.out.println("lent");
        } else
            System.out.println("not exist");
    }

    //deliver a book
    private static void deliver(long id, String bookId) {
        final Book book = books.get(bookId);
        book.setPersonThatBorrowedId(-1);
        book.setBorrowDate(false);
        members.get(id).removeBook(book);
    }

    /**
     * @return list of members that should pay fine
     */
    //find members that should pay fine
    private static ArrayList<Person> payFine(){
        ArrayList<Person> shouldPayFine = new ArrayList<>();
        DateTimeFormatter dateTimeFormatter= DateTimeFormatter.ofPattern("yyyy/MM/dd");
        final String now = dateTimeFormatter.format(LocalDateTime.now());
        books.forEach((k,v) -> {
                if (compareDate(now,v.getBorrowDate()) && v.getBorrowDate()!=null)
                    shouldPayFine.add(members.get(v.getPersonThatBorrowedId()));
        });
        return shouldPayFine;
    }

    //extend (tamdid)
    private static void extend(String bookId){
        books.get(bookId).setBorrowDate(true);
    }
    /**
     * @param now is today's date
     * @param borrowDate is date that member is borrowed the book
     */

    //compare borrow date and now for finding members should pay fine
    private static boolean compareDate(String now , String borrowDate){
        int yearNow = Integer.parseInt(now.substring(0,4));
        int yearBorrowDate =Integer.parseInt(borrowDate.substring(0,4));

        int monthNow = now.charAt(5)=='0' ? now.charAt(6)-48 : Integer.parseInt(now.substring(5,7));
        int monthBorrow = borrowDate.charAt(5)=='0' ? borrowDate.charAt(6)-48 : Integer.parseInt(borrowDate.substring(5,7));

        int dayNow = now.charAt(8)=='0' ? now.charAt(9)-48 : Integer.parseInt(now.substring(8,10));
        int dayBorrow = borrowDate.charAt(8)=='0' ? borrowDate.charAt(9)-48 : Integer.parseInt(borrowDate.substring(8,10));

        //20 days allowed
        if ((yearNow*356 + monthNow*30 +dayNow )-(yearBorrowDate*365 + monthBorrow*30 + dayBorrow) > 20)
            return true;
        else
            return false;
    }
}
