import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class FirstProject {
    static long id = 0;
    static Map<Long, Person> members;
    static Map<String, Book> books;
    static Scanner s = new Scanner(System.in);

    public static void main(String[] args) {
        members = new HashMap<>();
        books = new HashMap<>();
        loop:
        while (true) {
            System.out.println("1.members");
            System.out.println("2.books");
            System.out.println("3.search");
            System.out.println("4.exit");
            int chosen = s.nextInt();
            int order;
            switch (chosen) {
                case 1:
                    System.out.println("1.add member");
                    System.out.println("2.remove member");
                    System.out.println("3.show information");
                    System.out.println("4.edit information");
                    order = s.nextInt();
                    switch (order) {

                        case 1:
                            s.nextLine();
                            System.out.println("enter name , age , gender");
                            addMember(s.nextLine(), s.nextShort(), s.next().charAt(0));
                            break;
                        case 2:
                            System.out.println("enter id");
                            removeMember(s.nextLong());
                            break;
                        case 3:
                            System.out.println("enter id");
                            show(s.nextLong());
                            break;
                        case 4:
                            System.out.println("enter id");
                            edit(s.nextLong());
                            break;
                        default:
                            System.out.println("wrong input");
                    }
                    break;
                case 2:
                    System.out.println("1.add book");
                    System.out.println("2.remove book");
                    System.out.println("3.borrow");
                    System.out.println("4.deliver");
                    order = s.nextInt();
                    switch (order) {
                        case 1:
                            System.out.println("enter name , id");
                            s.nextLine();
                            addBook(s.nextLine(), s.next());
                            break;
                        case 2:
                            System.out.println("enter bookId");
                            s.nextLine();
                            removeBook(s.nextLine());
                            break ;
                        case 3:
                            System.out.println("enter member id , enter book id");
                            borrow(s.nextLong(), s.next());
                            break;
                        case 4:
                            System.out.println("enter member id , enter book id");
                            deliver(s.nextLong(), s.next());
                            break;
                        default:
                            System.out.println("wrong input");
                    }
                    break;
                case 3:
                    System.out.println("1.search exactly");
                    System.out.println("2.search contains");
                    System.out.println("3.search by book");
                    order = s.nextInt();
                    switch (order) {
                        case 1:
                            System.out.println("enter name");
                            s.nextLine();
                            searchByNameExactly(s.nextLine());
                            break;
                        case 2:
                            System.out.println("enter name");
                            s.nextLine();
                            searchByNameContains(s.nextLine());
                            break;
                        case 3:
                            System.out.println("enter bookId");
                            searchByBook(s.next());
                            break;
                        default:
                            System.out.println("wrong input");
                    }
                    break;
                case 4:
                    break loop;
                default:
                    System.out.println("wrong input");
            }
        }
    }
    //----------------------------- about members ------------------------------

    //add new member
    static void addMember(String name, short age, char gender) {
        members.put(id, new Person(name, age, gender, id++));
        System.out.println("your id : " + (id - 1));
    }

    //remove a member
    static void removeMember(long id) {
        if (members.remove(id, members.get(id))) {
            System.out.println("removed correctly");
        } else
            System.out.println("not exist");
    }

    //show members information
    static void show(long id) {
        if (members.containsKey(id)) {
            System.out.println(members.get(id));
        } else
            System.out.println("not exist");
    }

    //edit information
    static void edit(long id) {
        if (!members.containsKey(id))
            System.out.println("not exist");
        else {
            Person person = members.get(id);
            System.out.println("enter name: ");
            s.nextLine();
            person.setName(s.nextLine());
            System.out.println("enter age: ");
            person.setAge(s.nextShort());
            System.out.println("enter gender");
            person.setGender(s.next().charAt(0));
            System.out.println(members.get(id));
        }
    }

    //--------------------------- about books ---------------------------------

    //add new book
    static void addBook(String name, String id) {
        books.put(id, new Book(name, id));
        System.out.println("book added");
    }
    static void removeBook(String id){
        books.remove(id);
    }

    //borrow books
    static void borrow(long id, String bookId) {
        if (books.containsKey(bookId)) {
            if (books.get(bookId).getPersonThatBorrowedId() == -1) {
                books.get(bookId).setPersonThatBorrowedId(id);
                members.get(id).addBook(books.get(bookId));
            } else
                System.out.println("lended");
        } else
            System.out.println("not exist");
    }

    //deliver a book
    static void deliver(long id, String bookId) {
        books.get(bookId).setPersonThatBorrowedId(-1);
        members.get(id).removeBook(bookId);
    }


    //--------------------------------search-------------------------------------

    static void searchByBook(String bookId) {
        System.out.println(members.get(books.get(bookId).getPersonThatBorrowedId()));
    }

    //search exactly members by name
    static void searchByNameExactly(String name) {
        members.forEach((k, v) -> {
            if (v.getName().equals(name)) {
                System.out.println(v);
            }
        });
    }

    //search members by name that contains the name
    static void searchByNameContains(String name) {
        members.forEach((k, v) -> {
            if (v.getName().contains(name)) {
                System.out.println(v);
            }
        });
    }

}


class Person {
    private String name;
    private short age;
    private char gender;
    private long id;
    private ArrayList<Book> borrowedBooksList = new ArrayList<>();

    public Person(String name, short age, char gender, long id) {
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.id = id;
    }

    public void addBook(Book book) {
        borrowedBooksList.add(book);
    }

    public void removeBook(String bookId) {
        Book remove=null;
        for (Book b:borrowedBooksList){
            if (b.getId().equals(bookId))
                remove=b;
        }
        borrowedBooksList.remove(remove);
        borrowedBooksList.forEach(v -> System.out.println(v));
    }

    public void setAge(short age) {
        this.age = age;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", gender=" + gender +
                '}';
    }
}

class Book {
    private String name;
    private String id;
    private long personThatBorrowedId = -1;

    public Book(String name, String id) {
        this.name = name;
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setPersonThatBorrowedId(long personThatBorrowedId) {
        this.personThatBorrowedId = personThatBorrowedId;
    }

    public long getPersonThatBorrowedId() {
        return personThatBorrowedId;
    }

    @Override
    public String toString() {
        return "Book{" +
                "name='" + name + '\'' +
                ", id='" + id + '\'' +
                ", personThatBorrowedId=" + personThatBorrowedId +
                '}';
    }
}
